package sdk

import "fmt"

type OpenApi struct {
	AppKey    string
	AppSecret string
	Version   string
	Client    *Client
}

// GetClient 获取请求客户端
func (api *OpenApi) GetClient() *OpenApi {
	client, err := NewClientWithAccessKey(api.AppKey, api.AppSecret, api.Version)
	if err != nil {
		return nil
	}
	api.Client = client
	return api
}

// 开放接口地址
// 文档地址 https://www.dataoke.com/kfpt/api-d.html
// @params map[string]string 请求参数
// @params ap 请求接口名
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) General(ap string, params map[string]string) (string, *Error) {
	apiUrl := fmt.Sprintf("https://openapi.dataoke.com/api/%s", ap)
	return api.Client.Request(apiUrl, params, GET)
}

// TwdToTwd 淘口令转淘口令
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=30
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) TwdToTwd(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/twd-to-twd"
	return api.Client.Request(apiUrl, params, GET)
}

// GetTbService 联盟搜索
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=13
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetTbService(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/get-tb-service"
	return api.Client.Request(apiUrl, params, GET)
}

// GetGoodsList 商品列表
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=5
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// ParseContent 淘系万能解析
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=33
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ParseContent(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/parse-content"
	return api.Client.Request(apiUrl, params, GET)
}

// ParseTaoKouLing 淘口令解析
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=26
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ParseTaoKouLing(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/parse-taokouling"
	return api.Client.Request(apiUrl, params, GET)
}

// ListSuperGoods 超级搜索
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=14
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListSuperGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/list-super-goods"
	return api.Client.Request(apiUrl, params, GET)
}

// OpGoodsList 9.9包邮精选
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=15
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) OpGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/nine/op-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// PullGoodsByTime 定时拉取
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=12
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) PullGoodsByTime(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/pull-goods-by-time"
	return api.Client.Request(apiUrl, params, GET)
}

// GetRankingList 各大榜单
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=6
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetRankingList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-ranking-list"
	return api.Client.Request(apiUrl, params, GET)
}

// GetDtkSearchGoods 大淘客搜索
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=9
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetDtkSearchGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-dtk-search-goods"
	return api.Client.Request(apiUrl, params, GET)
}

// PromotionUnionConvert 京东商品转链
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=52
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) PromotionUnionConvert(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/kit/promotion-union-convert"
	return api.Client.Request(apiUrl, params, GET)
}

// GetPrivilegeLink 高效转链
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=7
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetPrivilegeLink(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/get-privilege-link"
	return api.Client.Request(apiUrl, params, GET)
}

// ListSimilarGoodsByOpen 猜你喜欢
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=16
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListSimilarGoodsByOpen(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/list-similer-goods-by-open"
	return api.Client.Request(apiUrl, params, GET)
}

// DdqGoodsList 咚咚抢
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=23
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) DdqGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/category/ddq-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// GetNewestGoods 商品更新
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=3
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetNewestGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-newest-goods"
	return api.Client.Request(apiUrl, params, GET)
}

// GetGoodsDetails 单品详情
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=8
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetGoodsDetails(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-goods-details"
	return api.Client.Request(apiUrl, params, GET)
}

// ExplosiveGoodsList 每日爆品推荐
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=34
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ExplosiveGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/explosive-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// HistoryPriceRecords 商品历史券后价
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=77
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) HistoryPriceRecords(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/stats/goods/historyPriceRecords"
	return api.Client.Request(apiUrl, params, GET)
}

// GetTop100 热搜记录
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=4
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetTop100(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/category/get-top100"
	return api.Client.Request(apiUrl, params, GET)
}

// SearchSuggestion 联想词
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=18
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) SearchSuggestion(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/search-suggestion"
	return api.Client.Request(apiUrl, params, GET)
}

// GetSuperCategory 超级分类
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=10
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetSuperCategory(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/category/get-super-category"
	return api.Client.Request(apiUrl, params, GET)
}

// GetOrderDetails 淘系订单查询
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=27
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetOrderDetails(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/get-order-details"
	return api.Client.Request(apiUrl, params, GET)
}

// GetStaleGoodsByTime 失效商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=11
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetStaleGoodsByTime(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-stale-goods-by-time"
	return api.Client.Request(apiUrl, params, GET)
}

// ActivityLink 官方活动会场转链
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=31
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ActivityLink(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/activity-link"
	return api.Client.Request(apiUrl, params, GET)
}

// ContentPromotionUnionBatchConvert 京东商品批量转链
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=78
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ContentPromotionUnionBatchConvert(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/kit/content/promotion-union-convert"
	return api.Client.Request(apiUrl, params, GET)
}

// ParseUrl 京东链接解析
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=75
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ParseUrl(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/kit/parseUrl"
	return api.Client.Request(apiUrl, params, GET)
}

// MaterialList 商品精推素材
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=74
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) MaterialList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/material/list"
	return api.Client.Request(apiUrl, params, GET)
}

// GoodsPromGenerate 拼多多商品转链
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=63
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GoodsPromGenerate(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/pdd/kit/goods-prom-generate"
	return api.Client.Request(apiUrl, params, GET)
}

// GetOfficialOrderList 京东订单查询
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=51
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetOfficialOrderList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/order/get-official-order-list"
	return api.Client.Request(apiUrl, params, GET)
}

// GetCouponInfo 优惠券查询
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=60
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetCouponInfo(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/taobao/kit/coupon/get-coupon-info"
	return api.Client.Request(apiUrl, params, GET)
}

// ExtraValueShopping 超值买返
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=58
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ExtraValueShopping(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/taobao/kit/turnLink/czmf"
	return api.Client.Request(apiUrl, params, GET)
}

// CreateTlj 淘礼金创建
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=56
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) CreateTlj(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/taobao/kit/create-tlj"
	return api.Client.Request(apiUrl, params, GET)
}

// GetCommentList 商品评论
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=43
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetCommentList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/comment/get-comment-list"
	return api.Client.Request(apiUrl, params, GET)
}

// ShopConvert 店铺转链
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=42
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ShopConvert(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/shop/convert"
	return api.Client.Request(apiUrl, params, GET)
}

// FirstOrderGiftMoney 首单礼金商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=29
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) FirstOrderGiftMoney(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/first-order-gift-money"
	return api.Client.Request(apiUrl, params, GET)
}

// CreateTaoKouLing 淘口令生成
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=28
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) CreateTaoKouLing(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/creat-taokouling"
	return api.Client.Request(apiUrl, params, GET)
}

// GetCollectionList 我的收藏
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=1
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetCollectionList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-collection-list"
	return api.Client.Request(apiUrl, params, GET)
}

// GetOwnerGoods 我发布的商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=2
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetOwnerGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-owner-goods"
	return api.Client.Request(apiUrl, params, GET)
}

// MergeRedEnvelopes 三合一红包
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=37
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) MergeRedEnvelopes(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/merge-red-envelopes"
	return api.Client.Request(apiUrl, params, GET)
}

// GetOrderList 京东一元购订单查询
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=50
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetOrderList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/order/outer/get-order-list"
	return api.Client.Request(apiUrl, params, GET)
}

// JdGoodsSearch 京东联盟搜索
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=70
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) JdGoodsSearch(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/goods/search"
	return api.Client.Request(apiUrl, params, GET)
}

// PddGoodsSearch 拼多多联盟搜索
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=71
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) PddGoodsSearch(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/pdd/goods/search"
	return api.Client.Request(apiUrl, params, GET)
}

// ListHotWords 热搜榜
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=61
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListHotWords(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/etc/search/list-hot-words"
	return api.Client.Request(apiUrl, params, GET)
}

// GetTbTopicList 官方活动(1元购)
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=24
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetTbTopicList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/category/get-tb-topic-list"
	return api.Client.Request(apiUrl, params, GET)
}

// CarouseList 轮播图
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=72
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) CarouseList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/topic/carouse-list"
	return api.Client.Request(apiUrl, params, GET)
}

// ListDiscountBrand 京东大牌折扣
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=67
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListDiscountBrand(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/column/list-discount-brand"
	return api.Client.Request(apiUrl, params, GET)
}

// ListTipOff 线报
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=62
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListTipOff(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/spider/list-tip-off"
	return api.Client.Request(apiUrl, params, GET)
}

// ListNines 京东9.9包邮
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=66
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListNines(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/column/list-nines"
	return api.Client.Request(apiUrl, params, GET)
}

// ListRealRanks 京东实时榜单
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=65
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListRealRanks(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/column/list-real-ranks"
	return api.Client.Request(apiUrl, params, GET)
}

// ListHeightCommission 高佣精选
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=59
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ListHeightCommission(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/singlePage/list-height-commission"
	return api.Client.Request(apiUrl, params, GET)
}

// GetHotAdvance 爆品预告商品合集
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=76
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetHotAdvance(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-hot-advance"
	return api.Client.Request(apiUrl, params, GET)
}

// AlbumList 专辑列表
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=53
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) AlbumList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/album/album-list"
	return api.Client.Request(apiUrl, params, GET)
}

// TopicGoodsList 专题商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=22
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) TopicGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/topic/goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// AlbumGoodsList 单个专辑商品列表
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=54
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) AlbumGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/album/goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// TopicCatalogue 精选专题
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=21
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) TopicCatalogue(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/topic/catalogue"
	return api.Client.Request(apiUrl, params, GET)
}

// ActivityGoodsList 活动商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=20
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ActivityGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/activity/goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// ActivityCatalogue 热门活动
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=19
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ActivityCatalogue(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/activity/catalogue"
	return api.Client.Request(apiUrl, params, GET)
}

// SubdivisionGetList 细分类目合集
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=68
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) SubdivisionGetList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/subdivision/get-list"
	return api.Client.Request(apiUrl, params, GET)
}

// ExtraValueShoppingGoodsList 超值买返商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=57
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ExtraValueShoppingGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/activity/list-goods-czmf"
	return api.Client.Request(apiUrl, params, GET)
}

// LiveGoodsList 热门主播力荐商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=55
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) LiveGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/live/goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// GetHistoryLowPriceList 历史新低商品合集
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=48
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetHistoryLowPriceList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-history-low-price-list"
	return api.Client.Request(apiUrl, params, GET)
}

// BrandGoodsList 单个品牌详情
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=45
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) BrandGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/delanys/brand/get-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// BrandColumnList 品牌栏目
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=44
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) BrandColumnList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/delanys/brand/get-column-list"
	return api.Client.Request(apiUrl, params, GET)
}

// SubdivisionRankList 细分类目榜
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=41
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) SubdivisionRankList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/subdivision/get-rank-list"
	return api.Client.Request(apiUrl, params, GET)
}

// SuperDiscountGoods 折上折
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=39
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) SuperDiscountGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/super-discount-goods"
	return api.Client.Request(apiUrl, params, GET)
}

// GetHalfPriceDayGoods 每日半价
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=38
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetHalfPriceDayGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/get-half-price-day"
	return api.Client.Request(apiUrl, params, GET)
}

// PriceTrendGoods 商品历史券后价
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=36
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) PriceTrendGoods(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/price-trend"
	return api.Client.Request(apiUrl, params, GET)
}

// LiveMaterialGoodsList 直播好货
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=35
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) LiveMaterialGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/liveMaterial-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// ExclusiveGoodsList 大淘客独家券商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=32
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) ExclusiveGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/exclusive-goods-list"
	return api.Client.Request(apiUrl, params, GET)
}

// FriendsCircleGoodsList 朋友圈文案
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=25
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) FriendsCircleGoodsList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/friends-circle-list"
	return api.Client.Request(apiUrl, params, GET)
}

// GetBrandList 品牌库
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=17
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) GetBrandList(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/tb-service/get-brand-list"
	return api.Client.Request(apiUrl, params, GET)
}

// JdOneDollarPurchase 京东年货节商品
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=40
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) JdOneDollarPurchase(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/goods/jd-one-dollar-purchase"
	return api.Client.Request(apiUrl, params, GET)
}

// JdGoodsDetails 京东商品详情
// 文档地址 https://www.dataoke.com/kfpt/api-d.html?id=64
// @params map[string]string 请求参数
// @return string 接口返回json字符串
// @return *Error 异常错误
func (api *OpenApi) JdGoodsDetails(params map[string]string) (string, *Error) {
	apiUrl := "https://openapi.dataoke.com/api/dels/jd/goods/get-details"
	return api.Client.Request(apiUrl, params, GET)
}
