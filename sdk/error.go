package sdk

import "encoding/json"

type Error struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func (e *Error) String() string {
	rs, _ := json.Marshal(e)
	return string(rs)
}
