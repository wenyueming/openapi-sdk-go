package sdk

import (
	"encoding/json"
	"fmt"
	"testing"
)

func Test_makeSign(t *testing.T) {
	var (
		appKey = "xxxxxxxx"
		appSecret = "xxxxxxxxxxxxx"
		version	 = "v1.0.2"
		params = make(map[string]string)
	)

	client, err := NewClientWithAccessKey(appKey, appSecret, version)
	if err != nil {
		fmt.Println(err)
		t.Failed()
	}
	params["page"] = "1"
	params["price"] = "1.12"
	sign, params := client.makeSign(params)
	fmt.Println("sign:", sign)
	rs, _ := json.MarshalIndent(params, " ", " ")
	fmt.Println(string(rs))
}
