# openapi-sdk-go

#### 介绍
dataoke openapi sdk for go

#### 安装教程

`go get -u gitee.com/dtk-developer/openapi-sdk-go/sdk`

#### 使用说明

```golang
package main

import (
	"fmt"
	"gitee.com/dtk-developer/openapi-sdk-go/sdk"
)

func main() {
	// 1.初始化所需要的参数appKey, appSecret, version
	var (
		appKey = "xxxxxxxxxx"
		appSecret = "xxxxxxxxxx"
		version = "v1.3.1"
	)

	// 2.初始化开放平台api的客户端句柄
	oa := &sdk.OpenApi{}
	oa.AppKey = appKey
	oa.AppSecret = appSecret
	oa.Version = version
	client :=  oa.GetClient()

	// 3.准备请求参数
	params := map[string]string {
		"goodsId": "639331017111",
	}

	// 4.业务请求，示例使用高效转链和淘系万能解析接口，其它开放接口使用方法相同
	resp, err := client.GetPrivilegeLink(params)
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)

	// 复用client句柄
	params = map[string]string {
		// 指定接口版本
		"version": "v1.0.0",
		"content": "1👈￥fym8XiJU9Oi￥ https://s.click.taobao.com/Ys3TNlu  包邮 漫花抽纸10包4层加厚280张/包纸巾卫生纸家用实惠装面巾纸",
	}
	resp, err = client.ParseContent(params)
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}
```
